import React from "react"
var _DummyComponent1_ = class extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isReady: false
        }
    }
    async componentDidMount() {
       let result = await $script.run("demopages.DummyScript", {});
       this.setState({isReady: true, name: result.Name});
    }
    render(){

        return (<div>
                    <h2>Hello World! {this.state.name}</h2>
                </div>);
    }
}

var DummyComponent1 = function(props) {
   return _DummyComponent1_
};
export {DummyComponent1};
export default DummyComponent1;

var states =  [];

var actions = {};

export {states};
export {actions};
/*srsti-uid=5dde94d0-bad5-11ea-a2a4-5b3a25027848##created_at=1593523615##updated_at=1593523615##created_by=System##updated_by=System*/
